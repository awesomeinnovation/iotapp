﻿angular.module('starter.services', [])

.factory('Staff', ['$http','$q', function($http, $q) {
  // Might use a resource here that returns a JSON array

  // Some fake testing data

  return {
    all: function() {
      var deferred = $q.defer();
      $http.get("https://webapplications-smiffykmc.c9users.io/api/staff")
      .then(deferred.resolve, deferred.reject);
      return deferred.promise;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  };
}])

.factory('Office', ['$http','$q', function ($http, $q) {
    // Might use a resource here that returns a JSON array

    // Some fake testing data

    return {
        all: function () {

          var deferred = $.defer();

            $http.get("https://webapplications-smiffykmc.c9users.io/api/tables")
            .then(deferred.resolve, deferred.reject);

            return deferred.promise;
        },
        remove: function (chat) {
            chats.splice(chats.indexOf(chat), 1);
        },
        get: function (tableId) {
          var deferred = $q.defer();
            $http
            .get("https://webapplications-smiffykmc.c9users.io/api/tables/" + tableId)
            .then(deferred.resolve, deferred.reject);
        },
        update: function (tableId, name) {
          var deferred = $q.defer();
            $http.post("https://webapplications-smiffykmc.c9users.io/api/tables/" + tableId + "/name/" + name)
            .then(deferred.resolve, deferred.reject);

            return deferred.promise;
        }
    };
}])
.service('MockDB', [function(){

  var db = {

      people: [{'id':01, 'name':'Robert', 'in':true},
                {'id':02, 'name':'Stephen', 'in':false},
                {'id':03, 'name':'Kieran', 'in':true},
                {'id':04, 'name':'Alan', 'in':false},
                {'id':05, 'name':'Joseph', 'in':true}],

      desks: [{'id':01, 'name':'Desk01', 'bookedBy':'Robert'},
              {'id':02, 'name':'Desk02', 'bookedBy':'Alan'},
              {'id':03, 'name':'Desk03', 'bookedBy':'Stephen'},
              {'id':04, 'name':'Desk04', 'bookedBy':null},
              {'id':05, 'name':'Desk05', 'bookedBy':'Kieran'}],

      getPeople: function() {
          return db.people;
      },

      getDesks: function() {
          return db.desks;
      }
  };
  return db;

}]);