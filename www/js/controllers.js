﻿angular.module('starter.controllers', ['ionic'])

.controller('DashCtrl', function($scope) {})
.controller('MenuCtrl', function($scope, $state){

    $scope.scan = function() {
        $state.go('menu.scan');
    };

})
.controller('StaffController', function($scope, $http, Staff, $timeout) {


    Staff.all().then(function(data){
        //console.log(data);
        $scope.staffList = data.staff;

        console.log(data.staff);
    });

    $scope.doRefresh = function () {

        console.log('Refreshing staff');

        Staff.all().then(function (data) {
                //console.log(data);
            $scope.staffList = data.staff;

                //Stop the ion-refresher from spinning
            $scope.$broadcast('scroll.refreshComplete');
        });
    };

    /*$scope.$on('$ionicView.beforeEnter', function (event, viewData) {
            viewData.enableBack = true;
        });*/


})

.controller('OfficeController', function ($scope, $http, Office, $timeout) {


    Office.all().success(function (data) {
            //console.log(data);
            $scope.officeList = data.tables;

            console.log(data.tables);
    });

    $scope.doRefresh = function () {

        console.log('Refreshing tables');


        Office.all().success(function (data) {
            //console.log(data);
            $scope.officeList = data.tables;

            //Stop the ion-refresher from spinning
            $scope.$broadcast('scroll.refreshComplete');
        });
    };

    $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
            viewData.enableBack = true;
        });

})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
})

    .controller('ScanCtrl', function($scope, $rootScope, $cordovaBarcodeScanner, $ionicPlatform) {
        var vm = this;

        vm.scan = function(){
            $ionicPlatform.ready(function() {
                $cordovaBarcodeScanner
                    .scan()
                    .then(function(result) {
                        // Success! Barcode data is here
                        vm.scanResults = "We got a barcode\n" +
                            "Result: " + result.text + "\n" +
                            "Format: " + result.format + "\n" +
                            "Cancelled: " + result.cancelled;
                    }, function(error) {
                        // An error occurred
                        vm.scanResults = 'Error: ' + error;
                    });
            });
        };

        vm.scanResults = '';
    })

    .controller("ScanController", function($scope, $cordovaBarcodeScanner, Office, Staff) {
        var id;

        $scope.table = false;

        $scope.selectables = [];

        $scope.scanBarcode = function() {
            $cordovaBarcodeScanner.scan().then(function(imageData) {
                alert(imageData.text);
                //$scope.scanResults = "Scan Worked " + imageData.text;
                var desk = JSON.parse(imageData.text);

                id = desk.tableID;

                Office.get(desk.tableID).success(function(d){

                    if($scope.selectables != null){
                        $scope.selectables = [];
                    }

                    var booked = d.table.BookedBy;

                    if(booked != null){
                        $scope.table = false;
                        $scope.scanResults = "Desk Already Occupied!";
                    } else {
                        $scope.scanResults = "Book Away!";
                        $scope.table = true;
                        Staff.all().then(function(staff){
                            staff.staff.forEach(function(staff){
                                if(staff.IN == true){
                                    $scope.selectables.push(staff);
                                }
                            });
                        }, function(reject){

                        });
                    }
                });
            }, function(error) {
                console.log("An error happened -> " + error);
            });
        };

        $scope.book = function(pick){
            //var pick = JSON.parse(name);
            //alert(pick);
            //$scope.scanResults = pick.UId;
            Office.update(id, pick.UId).success(function(data){
                alert("Successfully Booked Desk");
                $scope.table = false;
                $scope.scanResults = "";
                $scope.someModel = null;
            });
        };

        $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
            viewData.enableBack = true;
        });

    });
